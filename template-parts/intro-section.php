<?php
$app_store = get_field('app_store', 'options');
$google_play = get_field('google_play', 'options');
?>
<section class="intro-section">
    <div class="intro-section__top"></div>
    <img class="intro-section__bg" src="<?php echo get_stylesheet_directory_uri() . '/dist/images/MID.png' ?>" alt="Intro image">
    <img class="drone drone--center" src="<?php echo get_stylesheet_directory_uri() . '/dist/images/Drone-center.png' ?>" alt="Drone">
    <img class="drone drone--left" src="<?php echo get_stylesheet_directory_uri() . '/dist/images/Drone-left.png' ?>" alt="Drone">
    <img class="drone drone--right" src="<?php echo get_stylesheet_directory_uri() . '/dist/images/Drone-right.png' ?>" alt="Drone">

    <div class="intro-section__bottom">
        <div class="share-button">
			<?php if ($app_store) : ?>
                <a href="<?php echo $app_store ?>" target="_blank" rel="noopener">
                    <img src="<?php echo get_stylesheet_directory_uri() . '/dist/images/app-store.png' ?>" alt="Download on App Store">
                </a>
			<?php endif; ?>
			<?php if ($google_play) : ?>
                <a href="<?php echo $google_play ?>" target="_blank" rel="noopener">
                    <img src="<?php echo get_stylesheet_directory_uri() . '/dist/images/google-play.png' ?>" alt="Download on Google Play">
                </a>
			<?php endif; ?>
        </div>
    </div>
</section>