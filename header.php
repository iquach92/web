<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, initial-scale=1.0, maximum-scale=5.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="theme-color" content="#0D0A1B">
	<?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
<header class="header">
    <nav class="header-nav">
        <a class="logo" href="<?php echo esc_url(home_url('/')); ?>" rel="home" title="<?php bloginfo('name'); ?>">
			<?php if(has_custom_logo()) : ?>
				<?php
				$custom_logo_id = get_theme_mod( 'custom_logo' );
				$image = wp_get_attachment_image_src( $custom_logo_id , 'full' );
				?>
                <img src="<?php echo $image[0] ?>" alt="<?php bloginfo( 'name' ); ?>" />
			<?php else : ?>
				<?php bloginfo( 'name' ); ?>
			<?php endif ?>
        </a>
    </nav>
</header>
