import '../scss/app.scss';
import gsap from "gsap";
import ScrollTrigger from "gsap/ScrollTrigger";

gsap.registerPlugin(ScrollTrigger);

let droneLeft = document.querySelector('.drone--left');
let droneCenter = document.querySelector('.drone--center');
let droneRight = document.querySelector('.drone--right');
let logo = document.querySelector('.logo');
let shareButton = document.querySelector('.share-button');
let introBottom = document.querySelector('.intro-section__bottom');
let introBg = document.querySelector('.intro-section__bg');

function onLoad() {
    let loadTl = gsap.timeline();
    loadTl
        .from(logo, {y:-200,opacity: 0, duration: 1}, 0)
        .from(shareButton, {y: 250, opacity: 0, duration: 1}, 0)
        .from(droneCenter, {x: 100, y: 300, ease: "power2.inOut", opacity: 0, duration: 1}, 0)
        .from(droneLeft, {x: -500, y: 300, ease: "power2.inOut", opacity: 0, duration: 1},0)
        .from(droneRight, {x: 500, y: 300, ease: "power2.inOut", opacity: 0, duration: 1}, 0)
        .set('.intro-section', {marginBottom: '70vh'});
}
document.addEventListener("DOMContentLoaded", () => onLoad());

let tl = gsap.timeline({
    scrollTrigger: {
        trigger: 'body',
        start: '30vh',
        scrub: true,
    },
})

tl.to(droneCenter, {x: '300', y: '-300', ease: "power2.inOut"}, 0)
    .to(droneLeft, {x: '200%', y: '20%'}, 0)
    .to(droneRight, {x: '-80%', y: '30%', scale: 1.3}, 0)
    .to(introBottom, {height: '80vh'}, 0);
    if (window.onresize && window.innerWidth > 1024) {
        tl.to(introBg, {scale: 0.85, top: '60%'}, 0);
    }
