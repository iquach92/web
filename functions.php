<?php
function init_script_and_styles() {

	// Enqueue the main Stylesheet.
	wp_enqueue_style( 'main-stylesheet', get_template_directory_uri() . '/dist/css/main.css', array(), filemtime( dirname( __FILE__ ) . '/dist/css/main.css' ), 'all' );

	// Deregister the jquery version bundled with WordPress.
	wp_deregister_script( 'jquery' );

	wp_enqueue_script( 'jquery', 'https://code.jquery.com/jquery-3.5.1.min.js', array(), '3.2.1', true );

	// Deregister the jquery-migrate version bundled with WordPress.
	wp_deregister_script( 'jquery-migrate' );

	// CDN hosted jQuery migrate for compatibility with jQuery 3.x
	wp_register_script( 'jquery-migrate', '//code.jquery.com/jquery-migrate-3.0.1.min.js', array('jquery'), '3.0.1', true );

	// Enqueue the main Script.
	wp_enqueue_script('main-js', get_template_directory_uri() . '/dist/js/app.js', array(), filemtime( dirname( __FILE__ ) . '/dist/js/app.js' ), true);

	// Deregister extra
	wp_dequeue_style('wp-block-library');
	wp_deregister_script('wp-embed');

}
add_action( 'wp_enqueue_scripts', 'init_script_and_styles' );

// Add custom logo to Customize
add_theme_support( 'custom-logo' );
// Add meta title
add_theme_support( 'title-tag' );

// Add Image Sizes
add_image_size('intro-bg', 1400, 999, false);

// Add Options page
if( function_exists('acf_add_options_page') ) {
	acf_add_options_page();
}

// Add ACF JSON
add_filter('acf/settings/save_json', 'my_acf_json_save_point');
function my_acf_json_save_point( $path ) {
	// update path
	$path = get_stylesheet_directory() . '/acf-json';
	// return
	return $path;
}