<?php get_header(); ?>
<?php while (have_posts()): the_post(); ?>
    <h1><?php the_title() ?></h1>
	<?php if (get_the_content()) : ?>
        <div class="content">
			<?php the_content(); ?>
        </div>
	<?php endif; ?>
<?php endwhile ?>
<?php get_footer(); ?>
