const path = require('path');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const BrowserSyncPlugin = require('browser-sync-webpack-plugin');
const minifyCSS = require('optimize-css-assets-webpack-plugin');
const minifyJS = require('uglifyjs-webpack-plugin');
const CopyPlugin = require('copy-webpack-plugin');


module.exports = {
    entry: './assets/js/app.js',
    output: {
        path: path.resolve(__dirname, 'dist'),
        filename: './js/app.js',
    },
    devtool: 'inline-source-map',
    module: {
        rules: [
            {
                test: /\.m?js$/,
                exclude: /(node_modules|bower_components)/,
                use: {
                    loader: 'babel-loader',
                    options: {
                        presets: ['@babel/preset-env']
                    }
                }
            },
            {
                test: /\.(s*)css$/,
                use: [
                    {
                        loader: MiniCssExtractPlugin.loader,
                        options: {
                            publicPath: '../',
                        },
                    },
                    {
                        loader: 'css-loader',
                        options: { sourceMap: true },
                    },
                    {
                        loader: 'postcss-loader',
                    },
                    {
                        loader: 'sass-loader',
                        options: {
                            sassOptions: {
                                sourceMap: true,
                                sourceMapContents: false,
                                outputStyle: 'compressed',
                            },
                        },
                    },
                ]
            },
            {
                test: /\.(png|svg|jpg|gif)$/,
                loader: 'file-loader',
                options: {
                    name: 'images/[name].[ext]'
                }
            },
        ],
    },
    optimization: {
        minimizer: [
            new minifyCSS(),
            new minifyJS(),
        ],
    },
    plugins: [
        new MiniCssExtractPlugin({
            filename: 'css/[name].css',
        }),
        new BrowserSyncPlugin({
            proxy: "http://web.loc/",
            host: 'localhost',
            port: 3000,
            files: [
                './.',
            ],
        }),
        new CopyPlugin({
            patterns: [
                { from: './assets/images', to: 'images' },
            ],
            options: {
                concurrency: 100,
            },
        }),
    ]
};